# Clang Enabled Go Image

Golang image with clang installed on it: http://apt.llvm.org/

## Using in Gitlab CI for Go memory sanitation

```bash
memory_sanitizer:
  stage: test
  image: iadgarov/go-clang
  script:
    - export CC=clang-5.0
    - go test -msan -short <pkg list>
```