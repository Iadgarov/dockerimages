# DockerImages

[![pipeline status](https://gitlab.com/Iadgarov/dockerimages/badges/master/pipeline.svg)](https://gitlab.com/Iadgarov/dockerimages/commits/master) [![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

---

A collection of Dockerfiles and a CI/CD pipeline to push them to dockerhub

## List of Docker images in this repo

| Name     | Description                                                                                        |
|----------|----------------------------------------------------------------------------------------------------|
| go-clang | Golang base image with clang installed on it. Useful for running memory sanitation on Go projects. |

